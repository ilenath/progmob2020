package com.example.progmob2020.CRUD;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.progmob2020.R;

public class MainMahasiswaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_main);

        Button btnGet = (Button)findViewById(R.id.btnGet);
        Button btnAdd = (Button)findViewById(R.id.btnCreate);
        Button btnEdt = (Button)findViewById(R.id.btnUpdate);
        Button btnDel = (Button)findViewById(R.id.btnDelete);

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMahasiswaActivity.this, GetAllMahasiswaActivity.class);
                startActivity(intent);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMahasiswaActivity.this, AddMahasiswaActivity.class);
                startActivity(intent);
            }
        });

        btnEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( MainMahasiswaActivity.this, UpdateMahasiswaActivity.class);
                startActivity(intent);
            }
        });

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMahasiswaActivity.this, DeleteMahasiswaActivity.class);
                startActivity(intent);
            }
        });
    }
}