package com.example.progmob2020.CRUD3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.example.progmob2020.Adapter.MatkulCRUDRecyclerAdapter;
import com.example.progmob2020.Model.MataKuliah;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetAllMatkulActivity extends AppCompatActivity {

    RecyclerView rvMk;
    MatkulCRUDRecyclerAdapter matkulAdapter;
    ProgressDialog pd;
    List<MataKuliah> matkulList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_get_all);

        rvMk = (RecyclerView)findViewById(R.id.rvGetMkAll);
        pd = new ProgressDialog(this);
        pd.setTitle("Loading");
        pd.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<MataKuliah>> call = service.getMatkul("72180239");

        call.enqueue(new Callback<List<MataKuliah>>() {
            @Override
            public void onResponse(Call<List<MataKuliah>> call, Response<List<MataKuliah>> response) {
                pd.dismiss();
                matkulList = response.body();
                matkulAdapter = new MatkulCRUDRecyclerAdapter(matkulList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(GetAllMatkulActivity.this);
                rvMk.setLayoutManager(layoutManager);
                rvMk.setAdapter(matkulAdapter);
            }

            @Override
            public void onFailure(Call<List<MataKuliah>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(GetAllMatkulActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}