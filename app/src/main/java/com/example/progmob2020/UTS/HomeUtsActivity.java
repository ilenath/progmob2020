package com.example.progmob2020.UTS;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.progmob2020.CRUD.MainMahasiswaActivity;
import com.example.progmob2020.CRUD2.MainDosenActivity;
import com.example.progmob2020.CRUD3.MainMatkulActivity;
import com.example.progmob2020.R;

public class HomeUtsActivity extends AppCompatActivity {

    String isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uts_home);

        TextView tampil = (TextView)findViewById(R.id.textViewWelcome);
        ImageButton btnOut = (ImageButton) findViewById(R.id.imageButtonLogout);
        ImageButton btnDsn = (ImageButton) findViewById(R.id.imageButtonDsn);
        ImageButton btnMhs = (ImageButton) findViewById(R.id.imageButtonMhs);
        ImageButton btnMk = (ImageButton) findViewById(R.id.imageButtonMatkul);

        Bundle b = getIntent().getExtras();
        String textHelp = b.getString("help_string");
        tampil.setText(textHelp);

        SharedPreferences pref = HomeUtsActivity.this.getSharedPreferences("pref_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        btnOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeUtsActivity.this, MainUtsActivity.class);
                startActivity(intent);
                isLogin = pref.getString("isLogin", "0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin", "1");
                }else{
                    editor.putString("isLogin","0");
                }
                editor.commit();
            }
        });

        btnMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeUtsActivity.this, MainMahasiswaActivity.class);
                startActivity(intent);
            }
        });

        btnDsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeUtsActivity.this, MainDosenActivity.class);
                startActivity(intent);
            }
        });

        btnMk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeUtsActivity.this, MainMatkulActivity.class);
                startActivity(intent);
            }
        });
    }
}