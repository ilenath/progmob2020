package com.example.progmob2020.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmob2020.Adapter.MahasiswaCardAdapter;
import com.example.progmob2020.Model.Mahasiswa;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

public class CardViewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view_test);

        //data dummy
        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLatihan);
        MahasiswaCardAdapter mahasiswaCardAdapter;
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Nathaanna Ilenne H","72180192", "085729206882");
        Mahasiswa m2 = new Mahasiswa("Nathaanna Ilenne H","72180192", "085729206882");
        Mahasiswa m3 = new Mahasiswa("Nathaanna Ilenne H","72180192", "085729206882");
        Mahasiswa m4 = new Mahasiswa("Nathaanna Ilenne H","72180192", "085729206882");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);

        mahasiswaCardAdapter = new MahasiswaCardAdapter(CardViewTestActivity.this);
        mahasiswaCardAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(CardViewTestActivity.this));
        rv.setAdapter(mahasiswaCardAdapter);
    }
}